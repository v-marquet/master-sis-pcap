#!/usr/bin/env ruby

require "./utils"


# cf https://en.wikipedia.org/wiki/Address_Resolution_Protocol#Packet_structure

def analyse_arp_packet packet
  # Hardware type
  htype = packet[0..1].unpack("S>")[0]
  msg = (htype == 1) ? "(ethernet)" : "(UNSUPPORTED, skipping packet)"
  puts "└──┬─ Hardware type: #{htype} #{msg}"
  return if htype != 1

  # Protocol type
  ptype = packet[2..3].unpack("S>")[0]
  msg = (ptype == 2048) ? "(IPv4)" : "(UNSUPPORTED, skipping packet)"
  puts "   ├─ Protocol type: 0x#{ptype.to_s(16)} #{msg}"
  return if ptype != 2048

  # Hardware address length 
  hlen = packet[4].unpack("C")[0]
  msg = (hlen == 6) ? "" : "(UNSUPPORTED, skipping packet)"
  puts "   ├─ Hardware address length: #{hlen} bytes #{msg}"
  return if hlen != 6

  # Protocol address length
  plen = packet[5].unpack("C")[0]
  msg = (plen == 4) ? "" : "(UNSUPPORTED, skipping packet)"
  puts "   ├─ Protocol address length: #{plen} bytes #{msg}"
  return if plen != 4

  # Operation
  oper = packet[6..7].unpack("S>")[0]
  puts "   ├─ Operation: #{if oper == 1 then "request" elsif oper == 2 then "reply" end} (#{oper})"

  # Sender hardware address
  sha = packet[8..13]
  puts "   ├─ Sender hardware address: #{mac_to_s sha}"

  # Sender protocol address
  spa = packet[14..17]
  puts "   ├─ Sender protocol address: #{ipv4_to_s spa}"

  # Target hardware address
  tha = packet[18..23]
  puts "   ├─ Target hardware address: #{mac_to_s tha}"

  # Target protocol address
  tpa = packet[24..27]
  puts "   ├─ Target protocol address: #{ipv4_to_s tpa}"
end
