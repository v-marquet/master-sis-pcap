#!/usr/bin/env ruby


def analyse_pcap_packet_header packet_header, verbose=true
  # typedef struct pcaprec_hdr_s {
  #     guint32 ts_sec;         /* timestamp seconds */
  #     guint32 ts_usec;        /* timestamp microseconds */
  #     guint32 incl_len;       /* number of octets of packet saved in file */
  #     guint32 orig_len;       /* actual length of packet */
  # } pcaprec_hdr_t;

  ts_sec   = packet_header[0..3].unpack("L")[0]
  ts_usec  = packet_header[4..7].unpack("L")[0]
  incl_len = packet_header[8..11].unpack("L")[0]
  orig_len = packet_header[12..15].unpack("L")[0]

  msg = (incl_len == orig_len) ? "" : "(recorded: #{incl_len} bytes)"
  puts "New packet, total size #{orig_len} bytes #{msg}" if verbose

  return incl_len
end
