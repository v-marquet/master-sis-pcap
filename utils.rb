#!/usr/bin/env ruby


# convert 4 bytes array to an IPv4 address string
def ipv4_to_s data
  addr_numbers = []

  for i in 0..3
    addr_numbers << data[i].unpack("C")[0].to_s
  end

  return addr_numbers.join "."
end


# convert a MAC address (binary) to string
def mac_to_s data
  addr_numbers = []

  for i in 0..5
    addr_numbers << data[i].unpack("C")[0].to_s(16)
  end

  return addr_numbers.join "."
end
