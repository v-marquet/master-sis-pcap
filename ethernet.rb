#!/usr/bin/env ruby

require "./ipv4"
require "./ipv6"
require "./arp"
require "./utils"


#   0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15
# +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
# |                                               |
# |               Destination Address             |
# |                                               |
# +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
# |                                               |
# |                 Source Address                |
# |                                               |
# +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
# |                   EtherType                   |
# +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
# |                Data (46 - 1500)               |
# |                                               |

class EthernetFrame
  def initialize frame
    @frame = frame
    @mac_dst = mac_to_s(frame[0..5])
    @mac_src = mac_to_s(frame[6..11])
    @ethertype = frame[12..13].unpack("S>")[0]  # '>' => force big endian
    if @ethertype >= 42 && @ethertype <= 1500
      @payload = frame[14..(14+@ethertype-1)]
    else
      @payload = frame[14..(frame.bytesize-1)]
    end
  end

  # define getters
  attr_reader :frame, :mac_dst, :mac_src, :ethertype, :payload

  def analyse
    puts "├─ MAC src = #{mac_src}, MAC dest = #{mac_dst}"
    if !(ethertype >= 42 && ethertype <= 1500)
      puts "├─ Payload length: #{frame.bytesize-1 - 14 + 1}"
    end

    # get upper layer protocol
    if ethertype >= 42 && ethertype <= 1500
      puts "├─ EtherType: contains packet length: #{ethertype}"
      ipv4_packet = IPv4Packet.new payload
      ipv4_packet.analyse
    elsif ethertype == 0x0800
      puts "├─ EtherType: IPv4"
      ipv4_packet = IPv4Packet.new payload
      ipv4_packet.analyse
    elsif ethertype == 0x0806
      puts "├─ EtherType: ARP"
      analyse_arp_packet payload
    elsif ethertype == 0x8100
      puts "└─ EtherType: 802.1q packet. NOT SUPPORTED. Skipping packet."
    elsif ethertype == 0x86DD
      puts "└─ EtherType: IPv6"
      analyse_ipv6_packet payload
    else
      puts "└─ EtherType unknown: #{ethertype} = 0x#{ethertype.to_s(16)}, skipping packet"
      # to add support for more EtherTypes,
      # see https://en.wikipedia.org/wiki/EtherType
    end
  end

  def ipv4_payload?
    return @ethertype == 0x0800 ? true : false
  end

  def ipv6_payload?
    return @ethertype == 0x86DD ? true : false
  end
end



