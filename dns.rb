#!/usr/bin/env ruby

require "./utils"


# Structure d'un paquet DNS
# cf http://tools.ietf.org/html/rfc1035 page 25

# +---------------------+
# |        Header       |
# +---------------------+
# |       Question      | the question for the name server
# +---------------------+
# |        Answer       | RRs answering the question
# +---------------------+
# |      Authority      | RRs pointing toward an authority
# +---------------------+
# |      Additional     | RRs holding additional information
# +---------------------+


def analyse_dns_packet packet
  # Structure d'un header de paquet DNS
  # cf http://tools.ietf.org/html/rfc1035 page 26

  #   0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15
  # +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
  # |                      ID                       |
  # +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
  # |QR|   Opcode  |AA|TC|RD|RA|   Z    |   RCODE   |
  # +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
  # |                    QDCOUNT                    |
  # +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
  # |                    ANCOUNT                    |
  # +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
  # |                    NSCOUNT                    |
  # +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
  # |                    ARCOUNT                    |
  # +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
  id = packet[0..1].unpack("S>")[0]  # used by the requester to match up replies to outstanding queries
  puts "      └──┬─ ID: #{id}"

  flags = packet[2..3].unpack("S>")[0]
  qr     =  (flags & 0b1000_0000_0000_0000) >> 15                       # Query or response
  opcode =   flags & 0b0111_1000_0000_0000  >> 11                       # Kind of query
  aa     = ((flags & 0b0000_0100_0000_0000) >> 10) == 1 ? true : false  # Authoritative Answer
  tc     = ((flags & 0b0000_0010_0000_0000) >> 9)  == 1 ? true : false  # TrunCation
  rd     = ((flags & 0b0000_0001_0000_0000) >> 8)  == 1 ? true : false  # Recursion Desired
  ra     = ((flags & 0b0000_0000_1000_0000) >> 7)  == 1 ? true : false  # Recursion Available
  z      =  (flags & 0b0000_0000_0111_0000) >> 4                        # Reserved for future use
  rcode  =   flags & 0b0000_0000_0000_1111                              # Response code

  if qr == 0  # query
    case opcode
      when 0
        puts "         ├─ QR = 0: QUERY (standard query)"
      when 1
        puts "         ├─ QR = 0: IQUERY (inverse query)"
      when 2
        puts "         ├─ QR = 0: STATUS (server status request)"
    end
  elsif qr == 1  # answer
    puts "         ├─ QR = 1: #{if aa then "authoritative answer" else "answer" end}"
  end

  puts "         ├─ Flags: #{flags_to_s tc, rd, ra}"

  qdcount = packet[4..5].unpack("S>")[0]  # number of entries in the question section
  puts "         ├─ QDCOUNT: #{qdcount}"

  ancount = packet[6..7].unpack("S>")[0]  # number of resource records in the answer section
  nscount = packet[8..9].unpack("S>")[0]  # number of name server resource records
                                          # in the authority records section
  arcount = packet[10..11].unpack("S>")[0]  # number of resource records in the additional records section


  # Structure du champ question d'un paquet DNS
  # cf http://tools.ietf.org/html/rfc1035 page 28

  #   0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15
  # +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
  # |                                               |
  # /                     QNAME                     /
  # /                                               /
  # +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
  # |                     QTYPE                     |
  # +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
  # |                     QCLASS                    |
  # +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
  offset = 12
  for i in 1..qdcount
    # QNAME: a domain name
    labels = []
    while true
      size = packet[offset].unpack("C")[0]
      offset += 1

      break if size == 0  # null byte, announce the end of labels

      label = packet[offset..(offset+(size-1))]
      labels << label
      offset += size
    end
    puts "         ├─ Domain: #{labels.join '.'}"

    # QTYPE: a two octet code which specifies the type of the query
    qtype = packet[offset..(offset+1)].unpack("S>")[0]
    offset += 2

    # QCLASS: a two octet code that specifies the class of the query
    qclass = packet[offset..(offset+1)].unpack("S>")[0]
    offset += 2
  end


  # Structure du champ réponse d'un paquet DNS
  # cf http://tools.ietf.org/html/rfc1035 page 29

  #   0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15
  # +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
  # |                                               |
  # /                                               /
  # /                      NAME                     /
  # |                                               |
  # +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
  # |                      TYPE                     |
  # +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
  # |                     CLASS                     |
  # +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
  # |                      TTL                      |
  # |                                               |
  # +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
  # |                   RDLENGTH                    |
  # +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--|
  # /                     RDATA                     /
  # /                                               /
  # +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
  puts "         ├─ ANCOUNT: #{ancount}"
  for i in 1..ancount
    # NAME: a domain name
    name, read = get_labels_at offset, packet
    offset += read
    puts "         ├─ Domain: #{name}"

    type = packet[offset..(offset+1)].unpack("S>")[0]
    puts "         ├─ Type: #{type_to_s type}"
    offset += 2

    class_ = packet[offset..(offset+1)].unpack("S>")[0]
    puts "         ├─ Class: #{class_to_s class_}"
    offset += 2

    ttl = packet[offset..(offset+3)].unpack("L")[0]
    puts "         ├─ TTL: #{ttl} seconds"
    offset += 4

    rdlength = packet[offset..(offset+1)].unpack("S>")[0]
    puts "         ├─ RDLENGTH: length of RDATA is #{rdlength}"
    offset += 2

    rdata = packet[offset..(offset+rdlength-1)]
    if type == 1  # A
      puts "         ├─ RDATA: #{ipv4_to_s rdata}"
    else
      puts "         ├─ RDATA: unsupported"
    end
  end

  # TODO: authority section, additional section
end

# returns a string with the flags of the header section
def flags_to_s tc, rd, ra
  flags = []
  flags << "TC" if tc
  flags << "RD" if rd
  flags << "RA" if ra
  return flags.size == 0 ? "none" : flags.join(", ")
end

# returns:
# 1. the domain name, located at offset `offset` in packet
#    cf RFC section 4.1.4. Message compression
# 2. the number of bytes read BEFORE dereferencing a pointer
def get_labels_at offset, packet
  first_byte = packet[offset].unpack("C")[0]
  read = 1

  if first_byte == 0
    return nil, read
  end

  comp_flag = (first_byte & 0b1100_0000) >> 6

  if comp_flag == 0b00  # label not compressed
    size = first_byte
    label = packet[(offset+1)..(offset+size)]
    read += size

    label_next, read_next = get_labels_at (offset+size+1), packet
    if label_next == nil
      return label, read + read_next
    else
      return label + "." + label_next, read + read_next
    end
  elsif comp_flag == 0b11  # label compressed
    offset_new = packet[offset..(offset+1)].unpack("S>")[0] & 0b0011_1111_1111_1111
    read += 1
    labels, read_next = get_labels_at offset_new, packet
    return labels, read
  else
    puts "ERROR: comp_flag = #{comp_flag}"
  end
end

# convert answer section TYPE value to it's string name
# cf http://www.zytrax.com/books/dns/ch15/#answer
def type_to_s type
  case type
    when 1  then return "A"
    when 2  then return "NS"
    when 5  then return "CNAME"
    when 6  then return "SOA"
    when 11 then return "WKS"
    when 12 then return "PTR"
    when 15 then return "MX"
    when 33 then return "SRV"
    when 38 then return "A6"
    else return "Unknown"
  end
end

# cf http://www.zytrax.com/books/dns/ch15/#answer
def class_to_s class_
  case class_
    when 1 then return "IN (internet)"
    else return "Unknown"
  end
end

