#!/usr/bin/env ruby

# bonne ressource: http://packetlife.net/blog/2010/jun/7/understanding-tcp-sequence-acknowledgment-numbers/


def follow_tcp_stream ethernet_frames
  # first we list the streams available
  streams = []
  for i in 0..(ethernet_frames.size-1)
    ethernet_frame = ethernet_frames[i]

    if ethernet_frame.ipv4_payload?
      ipv4_packet = IPv4Packet.new ethernet_frame.payload
      if ipv4_packet.tcp_payload?
        tcp_packet = TCPPacket.new ipv4_packet.payload
        if tcp_packet.syn? && !tcp_packet.ack?
          puts "[Packet #{i}] New TCP connection (SYN): #{ipv4_packet.ip_src}:#{tcp_packet.port_src} => #{ipv4_packet.ip_dst}:#{tcp_packet.port_dst}, SEQ=#{tcp_packet.sequence_number}, ACK=#{tcp_packet.acknowledgment_number}"
          streams << i
        end
      end
    end
  end

  # we ask user which stream he wants to follow
  print "Enter the packet number of the stream you want to follow: "
  syn_packet_number = STDIN.gets.chomp.to_i

  if !streams.include?(syn_packet_number)
    puts "error: bad stream number"
    exit 1
  end

  # so now we have a stream to follow
  stream_ip_src = IPv4Packet.new(ethernet_frames[syn_packet_number].payload).ip_src
  stream_ip_dst = IPv4Packet.new(ethernet_frames[syn_packet_number].payload).ip_dst
  stream_port_src = TCPPacket.new(IPv4Packet.new(ethernet_frames[syn_packet_number].payload).payload).port_src
  stream_port_dst = TCPPacket.new(IPv4Packet.new(ethernet_frames[syn_packet_number].payload).payload).port_dst
  seq = TCPPacket.new(IPv4Packet.new(ethernet_frames[syn_packet_number].payload).payload).sequence_number
  ack = TCPPacket.new(IPv4Packet.new(ethernet_frames[syn_packet_number].payload).payload).acknowledgment_number
  puts "[Packet #{syn_packet_number}] SYN: SEQ = #{seq} (0x#{seq.to_s(16)}), ACK = #{ack} (0x#{ack.to_s(16)})"

  # first, we search the SYN ACK
  for i in syn_packet_number..(ethernet_frames.size-1)
    ethernet_frame = ethernet_frames[i]

    if ethernet_frame.ipv4_payload?
      ipv4_packet = IPv4Packet.new ethernet_frame.payload
      if ipv4_packet.tcp_payload?
        tcp_packet = TCPPacket.new ipv4_packet.payload
        if tcp_packet.syn? && tcp_packet.ack? && tcp_packet.acknowledgment_number == (seq + 1)
          syn_ack_packet_number = i
          puts "[Packet #{i}] Found SYN ACK: SEQ=#{tcp_packet.sequence_number} (0x#{tcp_packet.sequence_number.to_s(16)}), ACK=#{tcp_packet.acknowledgment_number} (0x#{tcp_packet.acknowledgment_number.to_s(16)})"
          ack = tcp_packet.acknowledgment_number
          seq = tcp_packet.sequence_number
          break
        end
      end
    end
  end

  # then, we search for the ACK
  for i in syn_ack_packet_number..(ethernet_frames.size-1)
    ethernet_frame = ethernet_frames[i]

    if ethernet_frame.ipv4_payload?
      ipv4_packet = IPv4Packet.new ethernet_frame.payload
      if ipv4_packet.tcp_payload?
        tcp_packet = TCPPacket.new ipv4_packet.payload
        if !tcp_packet.syn? && tcp_packet.ack? && tcp_packet.acknowledgment_number == (seq+1) && tcp_packet.sequence_number == ack
          ack_packet_number = i
          puts "[Packet #{i}] Found ACK: SEQ=#{tcp_packet.sequence_number} (0x#{tcp_packet.sequence_number.to_s(16)}), ACK=#{tcp_packet.acknowledgment_number} (0x#{tcp_packet.acknowledgment_number.to_s(16)})"
          ack = tcp_packet.acknowledgment_number
          seq = tcp_packet.sequence_number
          break
        end
      end
    end
  end

  # now we loop over all the other packets
  # to get all packets in two categories: client -> server and server -> client
  packets_from_server = []
  packets_from_client = []

  for i in (ack_packet_number+1)..(ethernet_frames.size-1)
    ethernet_frame = ethernet_frames[i]

    if ethernet_frame.ipv4_payload?
      ipv4_packet = IPv4Packet.new ethernet_frame.payload
      if ipv4_packet.tcp_payload?
        tcp_packet = TCPPacket.new ipv4_packet.payload

        # check if packet from client to server
        if ipv4_packet.ip_src == stream_ip_src && ipv4_packet.ip_dst == stream_ip_dst && tcp_packet.port_src == stream_port_src && tcp_packet.port_dst == stream_port_dst
          packets_from_client.push tcp_packet
          puts "[Packet #{i}] client -> server: SEQ=#{tcp_packet.sequence_number} (0x#{tcp_packet.sequence_number.to_s(16)}), ACK=#{tcp_packet.acknowledgment_number} (0x#{tcp_packet.acknowledgment_number.to_s(16)})"
        end

        # check if packet from server to client
        if ipv4_packet.ip_src == stream_ip_dst && ipv4_packet.ip_dst == stream_ip_src && tcp_packet.port_src == stream_port_dst && tcp_packet.port_dst == stream_port_src
          packets_from_server.push tcp_packet
          puts "[Packet #{i}] server -> client: SEQ=#{tcp_packet.sequence_number} (0x#{tcp_packet.sequence_number.to_s(16)}), ACK=#{tcp_packet.acknowledgment_number} (0x#{tcp_packet.acknowledgment_number.to_s(16)})"
        end
      end
    end
  end

  puts "Packets from client to server: #{packets_from_client.size}"
  puts "Packets from server to client: #{packets_from_server.size}"

  # we sort the packets
  packets_from_server.sort! do |a,b|
    if a.sequence_number != b.sequence_number  # first try to sort on sequence_number
      a.sequence_number <=> b.sequence_number
    else  # else we sort of acknowledgment_number
      a.acknowledgment_number <=> b.acknowledgment_number
    end
  end

  packets_from_client.sort! do |a,b|
    if a.sequence_number != b.sequence_number  # first try to sort on sequence_number
      a.sequence_number <=> b.sequence_number
    else  # else we sort of acknowledgment_number
      a.acknowledgment_number <=> b.acknowledgment_number
    end
  end

  # we display all data in direction client -> server and then server -> client,
  # if it's clear text
  if stream_port_dst == 80  # HTTP connection so clear text
    puts "="*30 + " DATA CLIENT -> SERVER " + "="*30
    for i in 0..(packets_from_client.size-1)
      puts packets_from_client[i].payload
    end

    puts "="*30 + " DATA SERVER -> CLIENT " + "="*30
    for i in 0..(packets_from_server.size-1)
      puts packets_from_server[i].payload
    end
  else
    puts "Can't display stream, not clear text"
  end
end
