#!/usr/bin/env ruby

require "./tcp"
require "./udp"
require "./icmp"
require "./utils"


# cf RFC 791
#  0                   1                   2                   3
#  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
# +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
# |Version|  IHL  |Type of Service|          Total Length         |
# +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
# |         Identification        |Flags|      Fragment Offset    |
# +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
# |  Time to Live |    Protocol   |         Header Checksum       |
# +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
# |                       Source Address                          |
# +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
# |                    Destination Address                        |
# +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
# |                    Options                    |    Padding    |
# +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

class IPv4Packet
  def initialize packet
    @version         = (packet[0].unpack("C")[0] & 0b11110000) >> 4
    @ihl             =  packet[0].unpack("C")[0] & 0b1111
    @type_of_service =  packet[1].unpack("C")[0]
    @total_length    =  packet[2..3].unpack("S>")[0]
    @identification  =  packet[4..5].unpack("S>")[0]
    @flags           = (packet[6].unpack("C")[0] & 0b11100000) >> 5
    @df = (@flags & 0b010) >> 1 == 1 ? true : false
    @mf = (@flags & 0b001)      == 1 ? true : false
    @fragment_offset =  packet[6..7].unpack("S>")[0] & 0b0001_1111_1111_1111
    @ttl             =  packet[8].unpack("C")[0]
    @protocole       =  packet[9].unpack("C")[0]
    @header_checksum =  packet[10..11].unpack("S>")[0]
    @ip_src = ipv4_to_s packet[12..15]
    @ip_dst = ipv4_to_s packet[16..19]

    # we read all options
    offset = 20
    for i in 0..(ihl-5-1)
      option = packet[(offset+i*4)..(offset+i*4+3)].unpack("L")[0]
      offset += 4
    end

    @payload = packet[offset..(@total_length-1)]
    @payload_length = total_length - offset
  end

  # define getters
  attr_reader :version, :ihl, :type_of_service, :total_length, :identification,
              :df, :mf, :fragment_offset, :ttl, :protocole, :ip_src, :ip_dst,
              :payload, :payload_length

  def analyse
    puts "└──┬─ IP version: #{version}"
    puts "   ├─ Internet Header Length: #{ihl} (#{ihl} words = #{ihl*4} bytes)"
    puts "   ├─ Total length: #{total_length} bytes"
    puts "   ├─ Identification: #{identification}"
    puts "   ├─ Flags: #{ipv4_flags_to_s(df, mf)}"
    puts "   ├─ Fragment offet: #{fragment_offset}"
    puts "   ├─ TTL: #{ttl}"
    puts "   ├─ Protocole: #{protocole_to_s protocole}"
    puts "   ├─ IP src = #{ip_src}, IP dest = #{ip_dst}"
    puts "   ├─ Payload length: #{payload_length}"

    if protocole == 1  # ICMP
      analyse_icmp_packet payload
    elsif protocole == 6  # TCP
      tcp_packet = TCPPacket.new payload
      tcp_packet.analyse
    elsif protocole == 17  # UDP
      analyse_udp_packet payload
    end
  end

  def icmp_payload?
    return (protocole == 1) ? true : false
  end

  def tcp_payload?
    return (protocole == 6) ? true : false
  end

  def udp_payload?
    return (protocole == 17) ? true : false
  end
end


# return the name of the protocole
def protocole_to_s protocole
  if protocole == 1
    return "ICMP"
  elsif protocole == 6
    return "TCP"
  elsif protocole == 17
    return "UDP"
  else
    return "unknown"
  end
end

# return a string with the name of flags
def ipv4_flags_to_s df, mf
  flags_s = ""
  flags_s += "DF (don't fragment) " if df
  flags_s += "MF (more fragments) " if mf
  flags_s = "none" if flags_s == ""
  return flags_s
end
