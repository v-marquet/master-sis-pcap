#!/usr/bin/env ruby

require "./protocoles"


# cf RFC 793
#  0                   1                   2                   3
#  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
# +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
# |          Source Port          |       Destination Port        |
# +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
# |                        Sequence Number                        |
# +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
# |             Acknowledgment Number (if ACK set)                |
# +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
# |  Data |Rese-|N|C|E|U|A|P|R|S|F|                               |
# | Offset|rved |S|W|C|R|C|S|S|Y|I|          Window size          |
# |       |     | |R|E|G|K|H|T|N|N|                               |
# +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
# |           Checksum            |         Urgent Pointer        |
# +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
# |                    Options                    |    Padding    |
# +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
# |                             data                              |
# +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

class TCPPacket
  def initialize packet
    @port_src              = packet[0..1].unpack("S>")[0]
    @port_dst              = packet[2..3].unpack("S>")[0]
    @sequence_number       = packet[4..7].unpack("L>")[0]
    @acknowledgment_number = packet[8..11].unpack("L>")[0]
    @data_offset           = packet[12].unpack("C")[0] & 0b11110000

    # flags
    @ns  =  (packet[12].unpack("C")[0] & 0b00000001)       == 1 ? true : false
    @cwr = ((packet[13].unpack("C")[0] & 0b10000000) >> 7) == 1 ? true : false
    @ece = ((packet[13].unpack("C")[0] & 0b01000000) >> 6) == 1 ? true : false
    @urg = ((packet[13].unpack("C")[0] & 0b00100000) >> 5) == 1 ? true : false
    @ack = ((packet[13].unpack("C")[0] & 0b00010000) >> 4) == 1 ? true : false
    @psh = ((packet[13].unpack("C")[0] & 0b00001000) >> 3) == 1 ? true : false
    @rst = ((packet[13].unpack("C")[0] & 0b00000100) >> 2) == 1 ? true : false
    @syn = ((packet[13].unpack("C")[0] & 0b00000010) >> 1) == 1 ? true : false
    @fin =  (packet[13].unpack("C")[0] & 0b00000001)       == 1 ? true : false

    @window_size    = packet[14..15].unpack("S>")[0]
    @checksum       = packet[16..17].unpack("S>")[0]
    @urgent_pointer = packet[18..19].unpack("S>")[0]

    @payload = packet[data_offset..(packet.bytesize-1)]
  end

  # define getters
  attr_reader :port_src, :port_dst, :sequence_number, :acknowledgment_number, :data_offset,
              :window_size, :checksum, :urgent_pointer, :payload

  def ns?; return @ns; end
  def cwr?; return @cwr; end
  def ece?; return @ece; end
  def urg?; return @urg; end
  def ack?; return @ack; end
  def psh?; return @psh; end
  def rst?; return @rst; end
  def syn?; return @syn; end
  def fin?; return @fin; end

  def analyse
    protocole_src = get_protocole_name(port_src) != nil ? " (#{get_protocole_name(port_src)})" : ""
    protocole_dst = get_protocole_name(port_dst) != nil ? " (#{get_protocole_name(port_dst)})" : ""
    puts "   └──┬─ Port src = #{port_src}#{protocole_src}, port dst = #{port_dst}#{protocole_dst}"
    puts "      ├─ Sequence number: #{sequence_number}"
    puts "      ├─ Acknowledgment number: #{acknowledgment_number}"
    puts "      ├─ Data offset: #{data_offset} (#{data_offset/4} bytes)"
    puts "      ├─ Flags: #{tcp_flags_to_s self}"
    puts "      ├─ Window size: #{window_size}"
    puts "      └─ Urgent pointer: #{urgent_pointer}" if urg?

    text_protocols = [21, 80]
    if text_protocols.include?(port_src) || text_protocols.include?(port_dst)
      puts "="*30 + " PAYLOAD DATA " + "="*30
      puts payload
      puts "="*74
    end
  end
end


def tcp_flags_to_s tcp_packet
  flags_s = []

  flags_s << "NS"  if ns?
  flags_s << "CWR" if cwr?
  flags_s << "ECE" if cwr?
  flags_s << "URG" if urg?
  flags_s << "ACK" if ack?
  flags_s << "PSH" if psh?
  flags_s << "RST" if rst?
  flags_s << "SYN" if syn?
  flags_s << "FIN" if fin?

  if flags_s.length == 0
    return "none"
  else
    return flags_s.join ', '
  end
end
