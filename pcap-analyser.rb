#!/usr/bin/env ruby

require "./pcap_global_header"
require "./pcap_packet_header"
require "./ethernet"
require "./follow_tcp_stream"


def main
  if ARGV.length < 1
    puts "Usage: #{$0} <file.pcap> [options]"
    exit 1
  end

  Signal.trap("PIPE", "EXIT")  # exit cleanly if user quits when using "| more"
  begin
    if ARGV.length == 1  # no options
      analyse ARGV[0]
    elsif ARGV[1] == "--follow-tcp-stream" || ARGV[1] == "-f"
      option_follow_tcp_stream ARGV[0]
    else
      puts "Unknown option"
    end
  rescue Interrupt  # exit cleanly if user press CTRL + C when using "| more"
    exit 0
  end
end

def analyse capture_filename
  File.open capture_filename do |f_in|
    # first we check that the file is a valid PCAP file, and we get info
    analyse_pcap_global_header f_in.read(24)

    # now we loop over each packet in the PCAP file
    while f_in.tell < f_in.size  # f_in.tell = current offset in file
      # we read the PCAP Record (Packet) Header
      packet_size = analyse_pcap_packet_header f_in.read(16)

      # we get the packet, and we analyse it
      packet = f_in.read(packet_size)
      ethernet_frame = EthernetFrame.new packet
      ethernet_frame.analyse

      puts ""
    end
  end
end

def option_follow_tcp_stream capture_filename
  File.open capture_filename do |f_in|
    # first we check that the file is a valid PCAP file, and we get info
    analyse_pcap_global_header f_in.read(24)

    # now we loop over each packet in the PCAP file
    ethernet_frames = []
    while f_in.tell < f_in.size  # f_in.tell = current offset in file
      # we read the PCAP Record (Packet) Header
      packet_size = analyse_pcap_packet_header f_in.read(16), false

      # we get the packet, and we analyse it
      packet = f_in.read(packet_size)
      ethernet_frame = EthernetFrame.new packet
      ethernet_frames << ethernet_frame
    end

    follow_tcp_stream ethernet_frames
  end
end

main()
