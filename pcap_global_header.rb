#!/usr/bin/env ruby


def analyse_pcap_global_header global_header
  # typedef struct pcap_hdr_s {
  #       guint32 magic_number;   /* magic number */
  #       guint16 version_major;  /* major version number */
  #       guint16 version_minor;  /* minor version number */
  #       gint32  thiszone;       /* GMT to local correction */
  #       guint32 sigfigs;        /* accuracy of timestamps */
  #       guint32 snaplen;        /* max length of captured packets, in octets */
  #       guint32 network;        /* data link type */
  # } pcap_hdr_t;

  puts "===== PCAP Global Header ====="

  magic_number = global_header[0..3].unpack("L")[0]
  puts "Magic number is 0x#{magic_number.to_s(16)}"
  if magic_number.to_s(16) != "a1b2c3d4"
    puts "ERROR: not a PCAP file (or endianness not supported), aborting."
    exit(1)
  end

  version_major = global_header[4..5].unpack("S")[0]
  version_minor = global_header[6..7].unpack("S")[0]
  puts "Version is #{version_major}.#{version_minor}"

  thiszone = global_header[8..11].unpack("l")[0]
  puts "GMT to local correction: #{thiszone}"

  sigfigs = global_header[12.15].unpack("L")[0]
  snaplen = global_header[16..19].unpack("L")[0]
  network = global_header[20..23].unpack("L")[0]

  puts "==============================\n\n"
end
