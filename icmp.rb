#!/usr/bin/env ruby


# cf RFC 792

#  0                   1                   2                   3
#  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
# +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
# |     Type      |     Code      |          Checksum             |
# +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
# |                             unused                            |
# +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
# |      Internet Header + 64 bits of Original Data Datagram      |
# +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

def analyse_icmp_packet packet
  type = packet[0].unpack("C")[0]
  puts "   └──┬─ Type: #{type} = #{type_to_s type}"

  code = packet[1].unpack("C")[0]
  puts "      ├─ Code: #{code} = #{code_to_s type, code}"

  checksum = packet[2..3].unpack("S>")[0]
end


def type_to_s type
  # classés dans l'ordre de la RFC
  case type
    when 3  then return "destination unreachable"
    when 11 then return "time exceeded"
    when 12 then return "parameter problem"
    when 4  then return "source quench"
    when 5  then return "redirect"
    when 8  then return "echo"
    when 0  then return "echo reply"
    when 13 then return "timestamp"
    when 14 then return "timestamp reply"
    when 15 then return "information request"
    when 16 then return "information reply"
    else         return "unknown"
  end
end

def code_to_s type, code
  if type == 3
    case code
      when 0 then return "net unreachable"
      when 1 then return "host unreachable"
      when 2 then return "protocol unreachable"
      when 3 then return "port unreachable"
      when 4 then return "fragmentation needed and DF set"
      when 5 then return "source route failed"
      else        return "unknown"
    end
  elsif type == 11
    case code
      when 0 then return "time to live exceeded in transit"
      when 1 then return "fragment reassembly time exceeded"
      else        return "unknown"
    end
  elsif type == 12
    case code
      when 0 then return "pointer indicates the error (cf RFC page 8)"
      else        return "unknown"
    end
  else
    return "unknown or not supported"
  end
end
