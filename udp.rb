#!/usr/bin/env ruby

require "./protocoles"
require "./dns"


# cf RFC 768
#  0      7 8     15 16    23 24    31  
# +--------+--------+--------+--------+ 
# |     Source      |   Destination   | 
# |      Port       |      Port       | 
# +--------+--------+--------+--------+ 
# |                 |                 | 
# |     Length      |    Checksum     | 
# +--------+--------+--------+--------+ 
# |                                     
# |          data octets ...            
# +---------------- ...                 

def analyse_udp_packet packet
  port_src = packet[0..1].unpack("S>")[0]
  port_dst = packet[2..3].unpack("S>")[0]
  protocole_src = get_protocole_name(port_src) != nil ? " (#{get_protocole_name(port_src)})" : ""
  protocole_dst = get_protocole_name(port_dst) != nil ? " (#{get_protocole_name(port_dst)})" : ""
  puts "   └──┬─ Port src = #{port_src}#{protocole_src}, port dst = #{port_dst}#{protocole_dst}"

  length = packet[4..5].unpack("S>")[0]
  puts "      ├─ Length: #{length}"

  checksum = packet[6..7].unpack("S>")[0]

  data = packet[8..(length-1)]

  if port_src == 53 || port_dst == 53  # 53 = DNS
    analyse_dns_packet data
  end
end
