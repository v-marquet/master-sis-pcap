#!/usr/bin/env ruby


def get_protocole_name port
  if port == 21
    return "FTP"
  elsif port == 23
    return "Telnet"
  elsif port == 25
    return "SMTP"
  elsif port == 53
    return "DNS"
  elsif port == 63
    return "Whois"
  elsif port == 80
    return "HTTP"
  elsif port == 110
    return "POP3"
  elsif port == 443
    return "HTTPS"
  else
    return nil
  end
end
